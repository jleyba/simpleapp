package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
)



// Get IP address
func myIPAddress() {
	addrs, err := net.InterfaceAddrs()

	if err != nil {
		//os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		//os.Exit(1)
		log.Fatal(err.Error())
		return
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				//os.Stdout.WriteString(ipnet.IP.String() + "\n")
				log.Println(ipnet.IP.String())
			}
		}
	}
}

func echoString(w http.ResponseWriter, r *http.Request) {

	myIPAddress()
	log.Println("Answered Hello")
	fmt.Fprintf(w, "hello")
}

func main() {
	http.HandleFunc("/", echoString)

	http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {

		myIPAddress()
		fmt.Fprintf(w, "Hi")
	})

	log.Println("Starting server....")

	log.Fatal(http.ListenAndServe(":8081", nil))

}
